const puppeteer = require('puppeteer');
const fs = require('fs');
const filename = 'keys.txt';

const key = 'поисковые запросы';
const count = 1000;

(async () => {
let keys = [];
  const browser = await puppeteer.launch({ headless: false,
      defaultViewport: null,
      slowMo:20,
      args: ['--no-sandbox',
      '--proxy-server=78.26.172.44:49344']});

  const page = await browser.newPage();
  await page.goto('https://google.com');
  keys.push(key);

  fs.writeFile(filename, "", function(err) {
    if(err) throw err;
    console.log("The file was created!");
  });
  for(let i = 0; keys.length < count && i < keys.length; i++){
    await page.click("input[name=q]");
    await page.type("input[name=q]", keys[i]);

    await page.waitForSelector('ul[role="listbox"]');
    const result = await page.evaluate(() => {
      let data = [];
      let elements =  document.getElementsByClassName('sbl1');
        for (var element of elements){
          if(element.textContent &&   element.textContent != document.querySelector('input[name=q]').value){
                  data.push(element.textContent);
          }
        }
        document.querySelector('input[name=q]').value = ''
        return data;
      });

      let all = result.filter(function(one) {
            return (keys.indexOf(one) == -1) ? true : false;
          });
      keys = keys.concat(all);

        fs.appendFile(filename, result.map(function(one){ return one+ '\n'}), function (err) {
          if (err) {
            return console.log(err);
          } else {
                console.log(  'status: ' + keys.length);
          }
        })

};

 browser.close();
})();
